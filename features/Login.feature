# Feature para hacer un login correcto
@login
Feature: Ejemplo de login de la página OragneHRM

  Background: Navegar a login
    Given el usuario navega a login

  Scenario: Login exitoso
    When el usuario introduce sus credenciales
    And el usuario pulsa el boton de login
    Then cerrar el navegador

  Scenario: Login falido
    When el usuario introduce sus credenciales erroneas
    And el usuario pulsa el boton de login
    Then se muestra un mensaje "Invalid credentials" en la pantalla
    And cerrar el navegador