import logging
import logging.handlers

'''
Clase Logger que nos sirve para registrar eventos en un archivo llamado
tests.log dentro de la carpeta logs.
'''


class Logger:

    @staticmethod
    def log_generator():

        # Le damos una configuración al Logger para saber dónde debe alojar tests.log
        # El formato, el modo escritura (w) y el formato de fecha (dia/mes/año hora:minutos:segundos)
        logging.basicConfig(
            filename="logs/tests.log",
            filemode='w',
            format='%(asctime)s - %(messages)s',
            datefmt='%d/%b/%y %H:%M:%S'
        )

        # Definimos cuantos archivos vamos a mantener y su tamaño
        r_file = logging.handlers.RotatingFileHandler(
            'logs/tests.log',
            backupCount=3,
            maxBytes=1024 * 1024 * 20
        )

        # Finalmente se define el logger que vamos a obtener y usar a lo largo del proyecto
        logger = logging.getLogger()
        logger.addHandler(r_file)
        logger.setLevel(logging.INFO) # Nivel de logging que queremos darle

        return logger
