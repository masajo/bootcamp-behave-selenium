import os
import json

settings = None


def load_settings():
    global settings
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'settings.json')) as settingsFile:
        settings = json.load(settingsFile)


# Llamamos a la función load_settings para
# cargamos el archivo settings.json para que se puedan consumir sus datos
# a lo largo del proyecto
load_settings()
