import time
from behave import given, when, then
from framework.WebFramework import webFramework
from pages.LoginPage import loginPage
from configurations.config import settings
from utilities.Logger import Logger

# Generamos el Logger
logger = Logger.log_generator()


# Implementar el GIVEN
@given(u'el usuario navega a login')
def navegar_login(context):
    logger.info('----- Navegador Carga y Navega a Login --------')
    webFramework.load_website()
    logger.info('----- Navegador Carga y Navega a Login CORRECTAMENTE --------')


@when(u'el usuario introduce sus credenciales')
def introducir_credenciales(context):
    logger.info('----- Usuario introduce sus credenciales --------')
    loginPage.insert_credentials()
    logger.info('----- Usuario introduce sus credenciales CORRECTAMENTE --------')


@when(u'el usuario introduce sus credenciales erroneas')
def introducir_credenciales(context):
    logger.info('----- Usuario introduce sus credenciales erróneas --------')
    loginPage.insert_wrong_credentials()
    logger.info('----- Usuario introduce sus credenciales erróneas CORRECTAMENTE --------')


@when(u'el usuario pulsa el boton de login')
def pulsar_boton_login(context):
    logger.info('----- Usuario pulsa botón de login --------')
    loginPage.click_login_button()
    logger.info('----- Usuario pulsa botón de login CORRECTAMENTE --------')


@then(u'cerrar el navegador')
def cerrar_navegador(context):
    logger.info('----- Cerrar Navegador --------')
    webFramework.close_browser()
    logger.info('----- Cerrar Navegador CORRECTAMENTE --------')


@then(u'se muestra un mensaje "Invalid credentials" en la pantalla')
def mostrar_mensaje_error(context):
    logger.info('----- Mostrar Error --------')
    time.sleep(2)
    loginPage.verify_error_credentials()
    logger.info('----- Mostrar Error CORRECTAMENTE --------')

